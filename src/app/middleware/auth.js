const jwt = require("jsonwebtoken");
const { mUser } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      throw new Error("Not Authenticated");
    }

    var strToken = null;
    if (authHeader.substring(0, 6) == "Bearer") {
      strToken = authHeader.substring(7, authHeader.length);
    } else {
      strToken = authHeader;
    }

    if (strToken == null) {
      throw new Error("Not Authenticated");
    }

    let decodedToken;
    try {
      decodedToken = jwt.verify(strToken, process.env.JWT_SECRET);
    } catch (err) {
      throw new Error("Not Authenticated");
    }
    if (!decodedToken) {
      throw new Error("Not Authenticated");
    }

    const user = await mUser
      .scope("withoutPassword")
      .findByPk(decodedToken.userId);
    if (!user) {
      throw new Error("Not Authenticated");
    }

    if (!user.active) {
      throw new Error("User is not active");
    }

    req.user = user;
    next();
  } catch (e) {
    // TODO: Error logging
    console.log(e);

    res.status(401).send({ error: e.message });
  }
};
