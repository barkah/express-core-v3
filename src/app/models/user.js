"use strict";
const mRole = require("./role");
const { includes } = require("lodash");

module.exports = (sequelize, DataTypes) => {
  const Role = mRole(sequelize, DataTypes);
  const user = sequelize.define(
    "user",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      username: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.TEXT,
      active: DataTypes.BOOLEAN,
      active_role: DataTypes.UUID,
      active_zone: DataTypes.UUID,
      last_login: DataTypes.DATE,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );
  user.addScope("withoutPassword", {
    attributes: { exclude: ["password"] },
  });

  // Protect Password
  user.prototype.toJSON = function () {
    var values = Object.assign({}, this.get());

    delete values.password;
    return values;
  };

  // Check if role user has access to module and action
  user.prototype.hasAccess = async function (module, action) {
    function camelCase(input) {
      return input.toLowerCase().replace(/-(.)/g, function (match, group1) {
        return group1.toUpperCase();
      });
    }

    const role = await Role.findByPk(this.active_role, {
      attributes: ["permissions"],
    });

    if (action instanceof Array) {
      const obj = {};
      for (var i in action) {
        let status = false;
        if (role.permissions[module]) {
          if (includes(role.permissions[module], action[i])) {
            status = true;
          }
        }
        obj[camelCase(action[i])] = status;
      }
      return obj;
    } else {
      let isAuth = true;
      if (role) {
        if (role.permissions[module]) {
          if (!includes(role.permissions[module], action)) {
            isAuth = false;
          }
        } else {
          isAuth = false;
        }
      } else {
        isAuth = false;
      }

      return isAuth;
    }
  };
  return user;
};
