"use strict";

module.exports = (sequelize, DataTypes) => {
  const userRole = sequelize.define(
    "user_role",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      user_id: DataTypes.UUID,
      zone_id: DataTypes.UUID,
      roles: DataTypes.JSON,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return userRole;
};
