const Sequelize = require("sequelize");
const db = require("../../utils/database");

const User = require("./user");
const Role = require("./role");
const Zone = require("./zone");
const UserRole = require("./user-role");
const UserLog = require("./user-log");
const Module = require("./module");
const Action = require("./action");
const FileCategory = require("./file-category");
const File = require("./file");

const mUser = User(db, Sequelize);
const mRole = Role(db, Sequelize);
const mZone = Zone(db, Sequelize);
const mUserRole = UserRole(db, Sequelize);
const mUserLog = UserLog(db, Sequelize);
const mModule = Module(db, Sequelize);
const mAction = Action(db, Sequelize);
const mFileCategory = FileCategory(db, Sequelize);
const mFile = File(db, Sequelize);

// Define Relations
mUser.hasMany(mUserRole, { foreignKey: "user_id" });
mZone.hasMany(mUserRole, { foreignKey: "zone_id" });

mUser.belongsTo(mRole, { foreignKey: "active_role" });
mUser.belongsTo(mZone, { foreignKey: "active_zone" });

mUserRole.belongsTo(mUser, {
  foreignKey: "user_id",
});
mUserRole.belongsTo(mZone, {
  foreignKey: "zone_id",
});

mUserLog.belongsTo(mUser, {
  foreignKey: "user_id",
});

mFileCategory.hasMany(mFile, { foreignKey: "file_category_id" });
mFile.belongsTo(mFileCategory, { foreignKey: "file_category_id" });

async function authenticate() {
  try {
    await db.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
}
authenticate();

module.exports = {
  mUser,
  mRole,
  mZone,
  mUserRole,
  mUserLog,
  mModule,
  mAction,
  mFileCategory,
  mFile,
};
