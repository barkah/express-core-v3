"use strict";

module.exports = (sequelize, DataTypes) => {
  const zone = sequelize.define(
    "zone",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      code: DataTypes.STRING,
      name: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.TEXT,
      lat: DataTypes.DOUBLE,
      lng: DataTypes.DOUBLE,
      level: DataTypes.STRING,
      parent: DataTypes.UUID,
      active: DataTypes.BOOLEAN,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return zone;
};
