"use strict";

module.exports = (sequelize, DataTypes) => {
  const userLog = sequelize.define(
    "user_log",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      user_id: DataTypes.UUID,
      lat: DataTypes.DOUBLE,
      lng: DataTypes.DOUBLE,
      browser: DataTypes.STRING,
      version: DataTypes.STRING,
      os: DataTypes.STRING,
      platform: DataTypes.STRING,
      type: DataTypes.STRING, // login/refresh
      expired_in: DataTypes.INTEGER,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return userLog;
};
