module.exports = function makePatchUser({ editUser }) {
  return async function patchUser(request) {
    try {
      if (!(await request.user.hasAccess("user", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const changes = request.body;

      const patched = await editUser({
        id: request.query.id,
        isAdmin: true,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
