module.exports = function makePostSwitchRole({ switchRole }) {
  return async function postSwitchRole(request) {
    try {
      const data = request.body;
      const user = request.user;

      const posted = await switchRole({
        data,
        user,
      });
      return {
        statusCode: 200,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
