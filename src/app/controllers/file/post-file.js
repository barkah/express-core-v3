const mimeType = require("./mimetype.json");

module.exports = function makePostFile({ addFile }) {
  return async function postFile(req, res, next) {
    try {
      const data = req.body;
      // console.log(data);
      const posted = await addFile({
        file: req.file,
        categoryId: req.params.fileCategory || null,
        mimeType,
        req,
        res,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
