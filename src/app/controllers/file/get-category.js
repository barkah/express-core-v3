const mimeType = require("./mimetype.json");

module.exports = function makeGetCategory({ singleCategory }) {
  return async function getCategory(request) {
    try {
      const slug = request.query.slug;

      const data = await singleCategory({
        slug,
        mimeType,
      });
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
