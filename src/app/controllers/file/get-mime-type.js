const mimeType = require("./mimetype.json");
const { forEach } = require("lodash");

module.exports = function makeGetMimeType() {
  return async function getMimeType(request) {
    try {
      const arrMime = [];

      forEach(mimeType, (value, key) => {
        arrMime.push({
          extension: key,
          mimetype: value,
        });
      });

      return {
        statusCode: 200,
        body: arrMime,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
