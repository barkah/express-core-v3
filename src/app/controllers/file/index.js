const {
  getFile,
  dataTableFile,
  addFile,
  removeFile,
  singleCategory,
} = require("../../usecases/file");

const makeLoadFile = require("./load-file");
const makePostFile = require("./post-file");
const makeDeleteFile = require("./delete-file");
const makeDataFile = require("./data-file");
const makeGetMimeType = require("./get-mime-type");
const makeGetCategory = require("./get-category");
const makeGetDefault = require("./get-default");

const loadFile = makeLoadFile({ getFile });
const postFile = makePostFile({ addFile });
const deleteFile = makeDeleteFile({ removeFile });
const dataFile = makeDataFile({ dataTableFile });
const getMimeType = makeGetMimeType();
const getCategory = makeGetCategory({ singleCategory });
const getDefault = makeGetDefault();

module.exports = Object.freeze({
  loadFile,
  dataFile,
  postFile,
  deleteFile,
  getMimeType,
  getCategory,
  getDefault,
});
