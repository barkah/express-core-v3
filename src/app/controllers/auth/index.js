const {
  loginWithPassword,
  refreshToken,
  register,
} = require("../../usecases/auth");

const makePostLoginWithPassword = require("./post-login-with-password");
const makePostRefreshToken = require("./post-refresh-token");
const makePostRegister = require("./post-register");

const postLoginWithPassword = makePostLoginWithPassword({ loginWithPassword });
const postRefreshToken = makePostRefreshToken({ refreshToken });
const postRegister = makePostRegister({ register });

module.exports = Object.freeze({
  postLoginWithPassword,
  postRefreshToken,
  postRegister,
});
