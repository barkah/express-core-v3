module.exports = function makePostRefreshToken({ refreshToken }) {
  return async function postRefreshToken(request) {
    try {
      const data = request.body;
      const authData = await refreshToken({
        data,
      });
      return {
        statusCode: 200,
        body: authData,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
