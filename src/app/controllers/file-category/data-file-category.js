module.exports = function makeDataFileCategory({ dataTableFileCategory }) {
  return async function dataFileCategory(request) {
    try {
      if (!(await request.user.hasAccess("file-category", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTableFileCategory({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
