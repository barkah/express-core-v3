module.exports = function makeDataAction({ dataTableAction }) {
  return async function dataAction(request) {
    try {
      if (!(await request.user.hasAccess("action", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTableAction({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
