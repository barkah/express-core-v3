const {
  singleAction,
  dataTableAction,
  addAction,
  editAction,
  removeAction,
  listAction,
} = require("../../usecases/action");

const makeGetAction = require("./get-action");
const makePostAction = require("./post-action");
const makePatchAction = require("./patch-action");
const makeDeleteAction = require("./delete-action");
const makeDataAction = require("./data-action");
const makeGetListAction = require("./get-list-action");

const getAction = makeGetAction({ singleAction });
const postAction = makePostAction({ addAction });
const patchAction = makePatchAction({ editAction });
const deleteAction = makeDeleteAction({ removeAction });
const dataAction = makeDataAction({ dataTableAction });
const getListAction = makeGetListAction({ listAction });

module.exports = Object.freeze({
  getAction,
  dataAction,
  postAction,
  patchAction,
  deleteAction,
  getListAction,
});
