const {
  singleRole,
  dataTableRole,
  addRole,
  editRole,
  removeRole,
  listRole,
} = require("../../usecases/role");

const makeGetRole = require("./get-role");
const makePostRole = require("./post-role");
const makePatchRole = require("./patch-role");
const makeDeleteRole = require("./delete-role");
const makeDataRole = require("./data-role");
const makeGetListRole = require("./get-list-role");

const getRole = makeGetRole({ singleRole });
const postRole = makePostRole({ addRole });
const patchRole = makePatchRole({ editRole });
const deleteRole = makeDeleteRole({ removeRole });
const dataRole = makeDataRole({ dataTableRole });
const getListRole = makeGetListRole({ listRole });

module.exports = Object.freeze({
  getRole,
  dataRole,
  postRole,
  patchRole,
  deleteRole,
  getListRole,
});
