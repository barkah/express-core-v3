const { singleUserLog, dataTableUserLog } = require("../../usecases/user-log");

const makeGetUserLog = require("./get-user-log");
const makeDataUserLog = require("./data-user-log");

const getUserLog = makeGetUserLog({ singleUserLog });
const dataUserLog = makeDataUserLog({ dataTableUserLog });

module.exports = Object.freeze({
  getUserLog,
  dataUserLog,
});
