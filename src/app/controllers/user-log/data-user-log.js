module.exports = function makeDataUserLog({ dataTableUserLog }) {
  return async function dataUserLog(request) {
    try {
      if (!(await request.user.hasAccess("user", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const userId = request.query.id || null;
      const data = await dataTableUserLog({
        params: request.body,
        userId,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
