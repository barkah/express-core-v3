module.exports = function makeGetZoneLevel() {
  return async function getZone(request) {
    try {
      const data = [
        { value: "kelurahan", text: "Kelurahan" },
        { value: "kecamatan", text: "Kecamatan" },
        { value: "kota", text: "Kota" },
        { value: "provinsi", text: "Provinsi" },
        { value: "skpd", text: "SKPD" },
        { value: "sub-skpd", text: "UKPD" },
      ];
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
