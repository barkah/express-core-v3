module.exports = function makeGetListModule({ listModule }) {
  return async function getListModule(request) {
    try {
      const data = await listModule();
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
