const {
  singleModule,
  dataTableModule,
  addModule,
  editModule,
  removeModule,
  listModule,
} = require("../../usecases/module");

const makeGetModule = require("./get-module");
const makePostModule = require("./post-module");
const makePatchModule = require("./patch-module");
const makeDeleteModule = require("./delete-module");
const makeDataModule = require("./data-module");
const makeGetListModule = require("./get-list-module");

const getModule = makeGetModule({ singleModule });
const postModule = makePostModule({ addModule });
const patchModule = makePatchModule({ editModule });
const deleteModule = makeDeleteModule({ removeModule });
const dataModule = makeDataModule({ dataTableModule });
const getListModule = makeGetListModule({ listModule });

module.exports = Object.freeze({
  getModule,
  dataModule,
  postModule,
  patchModule,
  deleteModule,
  getListModule,
});
