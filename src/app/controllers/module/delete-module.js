module.exports = function makeDeleteModule({ removeModule }) {
  return async function deleteModule(request) {
    try {
      if (!(await request.user.hasAccess("module", "delete"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const deleted = await removeModule({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
