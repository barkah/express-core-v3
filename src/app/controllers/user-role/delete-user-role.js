module.exports = function makeDeleteUserRole({ removeUserRole }) {
  return async function deleteUserRole(request) {
    try {
      if (!(await request.user.hasAccess("user-role", "delete"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const deleted = await removeUserRole({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
