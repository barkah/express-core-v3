const { mAction } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeAction({ validator }) {
  return async function makeAction({ id, slug = null, name = null } = {}) {
    if (!slug) {
      throw new Error("Action slug is required ");
    }
    if (!name) {
      throw new Error("Action name is required");
    }

    slug = slug.trim();
    name = name.trim();

    const _where = {
      slug: {
        [Op.iLike]: slug,
      },
    };

    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    const slugExists = await mAction.findOne({
      where: _where,
    });
    if (slugExists) {
      throw new Error("Slug " + slug + " already exists");
    }

    if (!validator.isSlug(slug)) {
      throw new Error("Invalid slug format");
    }

    return Object.freeze({
      getId: () => id,
      getSlug: () => slug,
      getName: () => name,
    });
  };
};
