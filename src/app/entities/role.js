const { mRole } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeRole({ validator }) {
  return async function makeRole({
    id,
    slug = null,
    name = null,
    default_route = null,
    permissions = null,
  } = {}) {
    if (!slug) {
      throw new Error("Role slug is required ");
    }
    if (!name) {
      throw new Error("Role name is required");
    }

    slug = slug.trim();
    name = name.trim();
    default_route = default_route ? default_route.trim() : null;

    const _where = {
      slug: {
        [Op.iLike]: slug,
      },
    };

    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    const slugExists = await mRole.findOne({
      where: _where,
    });
    if (slugExists) {
      throw new Error("Slug " + slug + " already exists");
    }

    if (!validator.isSlug(slug)) {
      throw new Error("Invalid slug format");
    }

    return Object.freeze({
      getId: () => id,
      getSlug: () => slug,
      getName: () => name,
      getDefaultRoute: () => default_route,
      getPermissions: () => permissions,
    });
  };
};
