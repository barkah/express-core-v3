const validator = require("validator");
const buildMakeModule = require("./module");
const buildMakeAction = require("./action");
const buildMakeZone = require("./zone");
const buildMakeRole = require("./role");
const buildMakeUser = require("./user");
const buildMakeUserRole = require("./user-role");
const buildMakeLogin = require("./login");

const makeModule = buildMakeModule({ validator });
const makeAction = buildMakeAction({ validator });
const makeZone = buildMakeZone({ validator });
const makeRole = buildMakeRole({ validator });
const makeUser = buildMakeUser({ validator });
const makeUserRole = buildMakeUserRole({ validator });
const makeLogin = buildMakeLogin({ validator });

module.exports = Object.freeze({
  makeModule,
  makeAction,
  makeZone,
  makeRole,
  makeUser,
  makeUserRole,
  makeLogin,
});
