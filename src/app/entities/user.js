const { mUser } = require("../models");
const { Op } = require("sequelize");
const bcrypt = require("bcryptjs");
const user = require("../models/user");

module.exports = function buildMakeUser({ validator }) {
  return async function makeUser({
    id,
    email = null,
    name = null,
    password = null,
    username = null,
    phone = null,
    address = null,
    active = true,
    active_role = null,
    active_zone = null,
    last_login = null,
    old_password = null,
    new_password = null,
  } = {}) {
    if (!email) {
      throw new Error("Email is required ");
    }
    if (!name) {
      throw new Error("Name is required");
    }

    if (!id && !password) {
      throw new Error("Password is required");
    }

    email = email.trim();
    name = name.trim();

    // Validate Email
    const _where = {
      email: {
        [Op.iLike]: email,
      },
    };

    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    const emailExists = await mUser.findOne({
      where: _where,
    });
    if (emailExists) {
      throw new Error("Email already exists");
    }

    if (!validator.isEmail(email)) {
      throw new Error("Invalid email format");
    }

    // Validate Active
    if (!validator.isBoolean(active.toString())) {
      throw new Error("Invalid boolean value for column active");
    }

    // if change password
    if (new_password && id) {
      if (!old_password) {
        throw new Error("Old password is required!");
      }

      // Validate Old Password
      if (old_password) {
        const userOld = await mUser.findByPk(id, {
          attributes: ["password"],
        });
        console.log(old_password, new_password, userOld.password);
        const passwordValid = await bcrypt.compare(
          old_password,
          userOld.password
        );
        if (!passwordValid) {
          throw new Error("Incorrect Password!");
        } else {
          password = new_password;
        }
      }
    }

    // Validate Password
    if (password) {
      if (
        !validator.isLength(password, { min: 6 }) ||
        !validator.matches(
          password,
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{6,})/
        )
      ) {
        throw new Error(
          "Password should have minimum six characters, at least one letter, one number and one special character"
        );
      }

      password = await bcrypt.hash(password, 12);
    }

    // Validate Username
    if (username) {
      username = username.toLowerCase();
      if (
        !validator.isLength(username, { min: 3, max: 100 }) ||
        !validator.matches(
          username,
          /^(?=[a-zA-Z0-9._]{3,100}$)(?!.*[_.]{2})[^_.].*[^_.]$/
        )
      ) {
        throw new Error(
          "Username only contains alphanumeric characters, underscore, dot and must between 3 to 100 character."
        );
      }

      const _whereUsername = {
        username: {
          [Op.iLike]: username,
        },
      };

      if (id) {
        _whereUsername.id = {
          [Op.ne]: id,
        };
      }
      const usernameExists = await mUser.findOne({
        where: _whereUsername,
      });
      if (usernameExists) {
        throw new Error("Username already exists");
      }
    }

    return Object.freeze({
      getId: () => id,
      getName: () => name,
      getEmail: () => email,
      getPassword: () => password,
      getUsername: () => username,
      getPhone: () => phone,
      getAddress: () => address,
      getActive: () => active,
      getActiveRole: () => active_role,
      getActiveZone: () => active_zone,
      getLastLogin: () => last_login,
    });
  };
};
