const { mZone } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeZone({ validator }) {
  return async function makeZone({
    id,
    code = null,
    name = null,
    phone = null,
    address = null,
    lat = null,
    lng = null,
    level = null,
    parent = null,
    active = true,
  } = {}) {
    if (!code) {
      throw new Error("Zone code is required ");
    }
    if (!name) {
      throw new Error("Zone name is required");
    }

    code = code.trim();
    name = name.trim();
    phone = phone ? phone.trim() : null;
    address = address ? address.trim() : null;

    const _where = {
      code: {
        [Op.iLike]: code,
      },
    };

    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    const codeExists = await mZone.findOne({
      where: _where,
    });
    if (codeExists) {
      throw new Error("Code " + code + " already exists");
    }

    if (lat && !validator.isDecimal(lat.toString())) {
      throw new Error("Invalid decimal value for lat");
    }

    if (lng && !validator.isDecimal(lng.toString())) {
      throw new Error("Invalid decimal value for lng");
    }

    if (!validator.isBoolean(active.toString())) {
      throw new Error("Invalid boolean value for active");
    }

    return Object.freeze({
      getId: () => id,
      getCode: () => code,
      getName: () => name,
      getPhone: () => phone,
      getAddress: () => address,
      getLat: () => lat,
      getLng: () => lng,
      getLevel: () => level,
      getParent: () => parent,
      getActive: () => active,
    });
  };
};
