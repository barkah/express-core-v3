module.exports = function makeRemoveRole({ mRole }) {
  return async function removeRole({ id }) {
    const removeRole = await mRole.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeRole,
    };
  };
};
