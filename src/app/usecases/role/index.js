const { mRole } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleRole = require("./single-role");
const makeDataTableRole = require("./datatable-role");
const makeAddRole = require("./add-role");
const makeEditRole = require("./edit-role");
const makeRemoveRole = require("./remove-role");
const makeListRole = require("./list-role");

const singleRole = makeSingleRole({ mRole });
const dataTableRole = makeDataTableRole({ datatable, mRole });
const addRole = makeAddRole({ mRole });
const editRole = makeEditRole({ mRole });
const removeRole = makeRemoveRole({ mRole });
const listRole = makeListRole({ mRole });

module.exports = Object.freeze({
  singleRole,
  dataTableRole,
  addRole,
  editRole,
  removeRole,
  listRole,
});
