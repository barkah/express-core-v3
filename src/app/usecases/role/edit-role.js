const { makeRole } = require("../../entities");

module.exports = function makeEditRole({ mRole }) {
  return async function editRole({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mRole.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find role with id " + id + ".");
    }

    const role = await makeRole({
      ...existing,
      ...changes,
    });

    await mRole.update(
      {
        slug: role.getSlug(),
        name: role.getName(),
        default_route: role.getDefaultRoute(),
        permissions: role.getPermissions(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
