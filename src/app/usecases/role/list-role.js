module.exports = function makeListRole({ mRole }) {
  return async function listRole() {
    const role = await mRole.findAll({
      attributes: ["id", "slug", "name"],
      order: [["name", "ASC"]],
      raw: true,
    });

    return role;
  };
};
