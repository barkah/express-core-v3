module.exports = function makeSingleModule({ mModule }) {
  return async function singleModule({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const module = await mModule.findByPk(id, { raw: true });

    if (!module) {
      throw new Error("Cannot find module with id " + id + ".");
    }

    return module;
  };
};
