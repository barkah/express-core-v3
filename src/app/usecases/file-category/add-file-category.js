const { makeFileCategory } = require("../../entities");

module.exports = function makeAddFileCategory({ mFileCategory }) {
  return async function addFileCategory({ data }) {
    const fileCategory = await makeFileCategory(data);

    const insertFileCategory = await new mFileCategory({
      slug: fileCategory.getSlug(),
      name: fileCategory.getName(),
      allowed_extension: fileCategory.getAllowedExtension(),
      max_file_size: fileCategory.getMaxFileSize(),
      active: fileCategory.getActive(),
    }).save();

    return {
      id: insertFileCategory.id,
    };
  };
};
