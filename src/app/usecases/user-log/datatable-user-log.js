const _ = require("lodash");
const { Op } = require("sequelize");

module.exports = function makeDataTableUserLog({ datatable, mUserLog }) {
  return async function ({ params, userId = null } = {}) {
    var dataTableObj = await datatable(params);
    dataTableObj.where = {
      [Op.and]: [
        dataTableObj.where,
        {
          user_id: userId,
        },
      ],
    };
    var data = await mUserLog.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
