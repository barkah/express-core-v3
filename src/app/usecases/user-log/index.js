const { mUserLog } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleUserLog = require("./single-user-log");
const makeDataTableUserLog = require("./datatable-user-log");

const singleUserLog = makeSingleUserLog({ mUserLog });
const dataTableUserLog = makeDataTableUserLog({
  datatable,
  mUserLog,
});

module.exports = Object.freeze({
  singleUserLog,
  dataTableUserLog,
});
