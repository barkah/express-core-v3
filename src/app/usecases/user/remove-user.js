module.exports = function makeRemoveUser({ mUser }) {
  return async function removeUser({ id }) {
    const removeUser = await mUser.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeUser,
    };
  };
};
