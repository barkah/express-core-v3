module.exports = function makeDataTableUser({
  datatable,
  mUser,
  mRole,
  mZone,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params, {
      onColumn: [
        {
          column: "role_name",
          model: "role",
          field: "name",
        },
        {
          column: "zone_name",
          model: "zone",
          field: "name",
        },
      ],
    });

    var data = await mUser.findAndCountAll({
      ...dataTableObj,
      include: [
        {
          model: mRole,
          attributes: [],
          required: false,
        },
        {
          model: mZone,
          attributes: [],
          required: false,
        },
      ],
    });

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
