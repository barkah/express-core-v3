module.exports = function makeSingleUser({ mUser }) {
  return async function singleUser({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const user = await mUser
      .scope("withoutPassword")
      .findByPk(id, { raw: true });

    if (!user) {
      throw new Error("Cannot find user with id " + id + ".");
    }

    return user;
  };
};
