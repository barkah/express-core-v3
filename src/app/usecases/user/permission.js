module.exports = function makePermission({ mUserRole, mRole }) {
  return async function permission({ user } = {}) {
    if (!user) {
      throw new Error("Not Authenticated");
    }

    if (user.active_zone && user.active_role) {
      const userRole = await mUserRole.findOne({
        attributes: ["roles"],
        where: {
          user_id: user.id,
          zone_id: user.active_zone,
        },
      });

      const currentRole = await mRole.findByPk(user.active_role);

      if (userRole && userRole.roles && currentRole) {
        if (userRole.roles.indexOf(currentRole.slug) !== -1) {
          return currentRole.permissions;
        }
      }
    }

    return null;
  };
};
