const { mUser, mUserRole, mRole, mZone } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleUser = require("./single-user");
const makeDataTableUser = require("./datatable-user");
const makeAddUser = require("./add-user");
const makeEditUser = require("./edit-user");
const makeRemoveUser = require("./remove-user");
const makePermission = require("./permission");
const makeListRole = require("./list-role");
const makeSwitchZone = require("./switch-zone");
const makeSwitchRole = require("./switch-role");

const singleUser = makeSingleUser({ mUser });
const dataTableUser = makeDataTableUser({ datatable, mUser, mRole, mZone });
const addUser = makeAddUser({ mUser });
const editUser = makeEditUser({ mUser });
const removeUser = makeRemoveUser({ mUser });
const permission = makePermission({ mUserRole, mRole });
const listRole = makeListRole({ mUserRole, mRole, mZone });
const switchZone = makeSwitchZone({ mZone, mUserRole, mRole, mUser });
const switchRole = makeSwitchRole({ mUserRole, mRole, mUser });

module.exports = Object.freeze({
  singleUser,
  dataTableUser,
  addUser,
  editUser,
  removeUser,
  permission,
  listRole,
  switchZone,
  switchRole,
});
