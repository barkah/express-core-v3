const { makeUser } = require("../../entities");

module.exports = function makeEditUser({ mUser }) {
  return async function editUser({ id, isAdmin = false, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }
    if (changes.password && !isAdmin) {
      delete changes.password;
    }

    const existing = await mUser.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find user with id " + id + ".");
    }

    const user = await makeUser({
      ...existing,
      ...changes,
    });
    const dataUpdate = {
      name: user.getName(),
      email: user.getEmail(),
      username: user.getUsername(),
      phone: user.getPhone(),
      address: user.getAddress(),
      active: user.getActive(),
    };
    if (user.getPassword() != null) {
      dataUpdate.password = user.getPassword();
    }
    await mUser.update(dataUpdate, { where: { id: id } });

    return {
      id: id,
    };
  };
};
