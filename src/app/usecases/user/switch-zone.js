module.exports = function makeSwitchZone({ mZone, mUserRole, mRole, mUser }) {
  return async function switchZone({ data, user }) {
    const zone = await mZone.findByPk(data.zone_id);

    if (zone) {
      const userRole = await mUserRole.findOne({
        attributes: ["id", "roles"],
        where: {
          user_id: user.id,
          zone_id: zone.id,
        },
      });

      if (userRole && userRole.roles && userRole.roles.length > 0) {
        const role = await mRole.findOne({
          where: { slug: userRole.roles[0] },
        });
        if (role) {
          await mUser.update(
            {
              active_zone: zone.id,
              active_role: role.id,
              updated_by: user.id,
            },
            { where: { id: user.id } }
          );

          return { to: role.default_route };
        } else {
          throw Error("Unknown Role");
        }
      } else {
        throw Error("Unknown Role");
      }
    } else {
      throw Error("Unknown Zone");
    }

    return null;
  };
};
