module.exports = function makeRemoveZone({ mZone }) {
  return async function removeZone({ id }) {
    const removeZone = await mZone.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeZone,
    };
  };
};
