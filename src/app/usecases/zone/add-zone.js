const { makeZone } = require("../../entities");

module.exports = function makeAddZone({ mZone }) {
  return async function addZone({ data }) {
    const zone = await makeZone(data);

    const insertZone = await new mZone({
      code: zone.getCode(),
      name: zone.getName(),
      phone: zone.getPhone(),
      address: zone.getAddress(),
      lat: zone.getLat(),
      lng: zone.getLng(),
      level: zone.getLevel(),
      parent: zone.getParent(),
      active: zone.getActive(),
    }).save();

    return {
      id: insertZone.id,
    };
  };
};
