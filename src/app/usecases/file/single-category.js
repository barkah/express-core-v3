const { map } = require("lodash");

module.exports = function makeSingleCategory({ mFileCategory }) {
  return async function singleCategory({ slug, mimeType } = {}) {
    if (!slug) {
      throw new Error("You must supply an slug.");
    }

    const fileCategory = await mFileCategory.findOne({
      attributes: ["id", "max_file_size", "slug", "allowed_extension"],
      where: {
        slug,
      },
    });

    if (!fileCategory) {
      throw new Error("Cannot find category " + slug + ".");
    }

    fileCategory.allowed_extension = map(
      fileCategory.allowed_extension,
      (val) => {
        return mimeType[val];
      }
    );

    return fileCategory;
  };
};
