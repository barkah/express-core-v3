const path = require("path");
const rimraf = require("rimraf");

module.exports = function makeRemoveFile({ mFile, mFileCategory }) {
  return async function removeFile({ id }) {
    const file = await mFile.findByPk(id);
    if (!file) {
      throw new Error("File not found");
    }

    const uploadPath = "./../../../../public/uploads/";
    let _path = uploadPath;
    if (file.file_category_id != null) {
      const fileCategory = await mFileCategory.findByPk(file.file_category_id);
      _path = _path + fileCategory.slug + "/";
    }

    const dir = path.join(__dirname + _path + file.id);

    await rimraf.sync(dir);

    const removeFile = await mFile.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeFile,
    };
  };
};
