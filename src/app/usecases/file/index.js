const { mFile, mFileCategory } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeGetFile = require("./get-file");
const makeDataTableFile = require("./datatable-file");
const makeAddFile = require("./add-file");
const makeRemoveFile = require("./remove-file");
const makeSingleCategory = require("./single-category");
const makeGetFileOfData = require("./get-file-of-data");

const getFile = makeGetFile({ mFile });
const dataTableFile = makeDataTableFile({ datatable, mFile, mFileCategory });
const addFile = makeAddFile({ mFileCategory, mFile });
const removeFile = makeRemoveFile({ mFile, mFileCategory });
const singleCategory = makeSingleCategory({ mFileCategory });
const getFileOfData = makeGetFileOfData({ mFile });

module.exports = Object.freeze({
  getFile,
  dataTableFile,
  addFile,
  removeFile,
  singleCategory,
  getFileOfData,
});
