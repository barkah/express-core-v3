const { map, find } = require("lodash");
const makeGetFile = require("./get-file");

module.exports = function makeGetFileOfData({ mFile }) {
  return async function getFileOfData({ data, documents } = {}) {
    const getFile = makeGetFile({ mFile });
    var listOfFileId = [];
    for (var i in data) {
      var dt = data[i];

      if (dt[documents] != null) {
        listOfFileId = [...listOfFileId, ...dt[documents]];
      }
    }

    if (listOfFileId.length > 0) {
      const listOfFile = await getFile({ fileIds: listOfFileId });
      if (listOfFile && listOfFile.length > 0) {
        data = map(data, (md) => {
          if (md[documents] && md[documents] != null) {
            md[documents] = map(md[documents], (doc) => {
              return find(listOfFile, {
                id: doc,
              });
            });
          }

          return md;
        });
      }
    }

    return data;
  };
};
