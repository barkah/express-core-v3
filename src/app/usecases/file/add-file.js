const uuid = require("uuid");
const multer = require("multer");
const path = require("path");
const fs = require("fs");
const config = require("../../../config/app");
const { map } = require("lodash");

module.exports = function makeAddFile({ mFileCategory, mFile }) {
  return async function addFile({
    file,
    categoryId = null,
    mimeType,
    req,
    res,
  }) {
    let category = null;
    let allowedFileType = config.allowedFileType;
    let limits = {
      files: 1,
      fileSize: config.maxFileSize,
    };

    if (categoryId) {
      category = await mFileCategory.findOne({
        where: {
          slug: categoryId,
        },
      });
    }

    // Make Category Directory
    const uploadPath = "./../../../../public/uploads/";
    let _path = uploadPath;
    if (category) {
      _path = _path + category.slug + "/";
      const dirCat = path.join(__dirname + _path);
      if (!fs.existsSync(dirCat)) {
        await fs.mkdirSync(dirCat);
      }

      if (category.max_file_size) {
        limits.fileSize = category.max_file_size * 1024;
      }

      if (category.allowed_extension) {
        allowedFileType = map(category.allowed_extension, (ext) => {
          return mimeType[ext];
        });
      }
    }

    const id = uuid.v4();

    // Make Directory
    const dir = path.join(__dirname + _path + id);
    if (!fs.existsSync(dir)) {
      await fs.mkdirSync(dir);
    }
    let newName = null;

    const storage = multer.diskStorage({
      destination: dir + "/",
      filename: function (req, file, cb) {
        // console.log(file);
        let ext = null;
        for (var tp in mimeType) {
          if (mimeType[tp] == file.mimetype) {
            ext = tp;
            break;
          }
        }
        const filename = file.originalname;
        var _name = filename.substr(0, filename.lastIndexOf("."));
        _name = _name.replace(/[^a-zA-Z0-9-_\ \.\_\(\)]+/gi, "");
        _name = _name.substring(0, 100);
        newName = _name + "." + ext;

        cb(null, newName);
      },
    });

    const upload = multer({
      storage: storage,
      limits: limits,
      fileFilter: (req, file, cb) => {
        if (allowedFileType.indexOf(file.mimetype) !== -1) {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(
            new Error("Only " + allowedFileType.join(", ") + " Allowed")
          );
        }
      },
    }).single("file");

    if (upload) {
      await upload(req, res, async (err) => {
        if (err) {
          throw new Error(err.message);
        }

        const reqFile = req.file;

        try {
          let fpath = id + "/" + newName;
          if (category) {
            fpath = category.slug + "/" + fpath;
          }
          const theFile = uploadPath + fpath;
          console.log(path.extname(theFile), theFile);
          const extname = path.extname(theFile).substr(1);

          const file = await new mFile({
            id: id,
            file_category_id: category ? category.id : null,
            name: newName,
            type: reqFile.mimetype,
            size: reqFile.size,
            path: "/" + fpath,
            extension: extname,
            createdBy: req.user ? req.user.id : null,
          }).save();

          res.send({
            id: file.id,
            name: file.name,
            type: file.type,
            size: file.size,
            url: file.url || config.fileUrl + file.path,
          });
        } catch (err) {
          throw new Error(err.message);
        }
      });
    }

    return {
      id: null,
    };
  };
};
