const { makeLogin } = require("../../entities");
const token = require("../../../utils/token");
const Sequelize = require("sequelize");

module.exports = function makeLoginWithPassword({
  mUser,
  mUserLog,
  mZone,
  mRole,
}) {
  return async function loginWithPassword({ data, agent }) {
    const login = await makeLogin(data);

    const user = login.getUser();

    await mUser.update(
      { last_login: Sequelize.fn("NOW") },
      { where: { id: user.id } }
    );

    await new mUserLog({
      user_id: user.id,
      browser: agent.browser,
      version: agent.version,
      os: agent.os,
      platform: agent.platform,
      type: "login-with-password",
      expiredIn: process.env.JWT_EXPIRES_IN,
      createdBy: user.id,
    }).save();

    const accesToken = new token("password").generate({
      userId: user.id,
      grantType: "password",
    });
    const refreshToken = new token("refresh").generate({
      userId: user.id,
      grantType: "password",
    });

    const zone = await mZone.findByPk(login.activeZone(), {
      attributes: ["id", "code", "name"],
      raw: true,
    });
    const role = await mRole.findByPk(login.activeRole(), {
      attributes: ["id", "slug", "name"],
      raw: true,
    });

    return {
      token_type: "bearer",
      access_token: accesToken,
      refresh_token: refreshToken,
      user: {
        id: user.id,
        email: user.email,
        name: user.name,
        username: user.username,
        activeZone: zone || null,
        activeRole: role || null,
      },
      to: login.to(),
      expires_in: process.env.JWT_EXPIRES_IN,
    };
  };
};
