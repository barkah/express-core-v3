const { makeUser } = require("../../entities");

module.exports = function makeRegister({ mUser }) {
  return async function register({ data }) {
    const user = await makeUser(data);

    const newUser = await new mUser({
      email: user.getEmail(),
      password: user.getPassword(),
      name: user.getName(),
      username: user.getUsername() || null,
    }).save();

    return {
      id: newUser.id,
    };
  };
};
