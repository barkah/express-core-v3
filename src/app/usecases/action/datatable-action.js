module.exports = function makeDataTableAction({ datatable, mAction }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mAction.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
