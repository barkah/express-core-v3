const { makeAction } = require("../../entities");

module.exports = function makeEditAction({ mAction }) {
  return async function editAction({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mAction.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find action with id " + id + ".");
    }

    const action = await makeAction({
      ...existing,
      ...changes,
    });

    await mAction.update(
      {
        slug: action.getSlug(),
        name: action.getName(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
