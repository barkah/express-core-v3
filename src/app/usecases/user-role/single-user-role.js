module.exports = function makeSingleUserRole({ mUserRole }) {
  return async function singleUserRole({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const userRole = await mUserRole.findByPk(id, { raw: true });

    if (!userRole) {
      throw new Error("Cannot find user role with id " + id + ".");
    }

    return userRole;
  };
};
