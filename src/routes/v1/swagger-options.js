const config = require("../../config/app");
const schemas = {
  permissionResponse: {
    type: "object",
    example: {
      user: ["view", "create", "update", "delete", "change-password"],
      role: ["view", "create", "update", "delete"],
    },
  },
  listRoleResponse: {
    type: "object",
    example: [
      {
        zone_id: "26201546-682c-4bf1-9d0a-8324af031d39",
        zone_name: "Default Zone",
        roles: [
          {
            id: "c2a88b18-4783-4fcc-8192-db32ec9aa297",
            slug: "administrator",
            name: "Administrator",
          },
        ],
      },
    ],
  },
  loginData: {
    type: "object",
    example: {
      email: "demo@demo.com",
      password: "AuxKo5E@",
    },
  },
  loginResponse: {
    type: "object",
    example: {
      token_type: "bearer",
      access_token: "...",
      refresh_token: "...",
      user: {
        id: "",
        email: "",
        name: "",
        username: "",
        activeZone: {
          id: "",
          code: "",
          name: "",
        },
        activeRole: {
          id: "",
          slug: "",
          name: "",
        },
      },
      to: "/",
      expires_in: "0",
    },
  },
  userRoleData: {
    type: "object",
    example: {
      user_id: "98f043e9-b33f-46ce-8bd9-a1ee45af1874",
      zone_id: "26201546-682c-4bf1-9d0a-8324af031d39",
      roles: ["administrator"],
    },
  },
  userData: {
    type: "object",
    example: {
      name: "Administrator 2",
      email: "admin@gmail.com",
      username: "admin",
      phone: "",
      address: "",
      active: true,
    },
  },
  roleData: {
    type: "object",
    example: {
      slug: "admin",
      name: "Administrator",
      default_route: "/",
      permissions: { user: ["create", "update", "delete"] },
    },
  },
  zoneData: {
    type: "object",
    example: {
      code: "12345",
      name: "Default Zone",
    },
  },
  moduleData: {
    type: "object",
    example: {
      slug: "action",
      name: "Action",
    },
  },
  actionData: {
    type: "object",
    example: {
      slug: "create",
      name: "Create",
    },
  },
  postResponse: {
    type: "object",
    example: {
      id: "54de80f4-fba4-4fcc-874f-2672a03f468e",
    },
  },
  deleteResponse: {
    type: "object",
    example: {
      deleted: 1,
    },
  },
  dataTablePost: {
    type: "object",
    example: {
      columns: [
        {
          data: "column_name",
        },
      ],
      order: {
        column: "column_name",
        dir: "asc",
      },
      page: 1,
      perPage: 10,
    },
  },
  dataTableResponse: {
    type: "object",
    example: {
      recordsTotal: 100,
      data: [],
    },
  },
};

module.exports = {
  swaggerDefinition: {
    info: {
      version: "version 1.0.0",
      title: "Framework V3",
      description: "Framework V3",
      contact: {
        name: "Barkah Hadi",
      },
      servers: [config.url],
    },
    basePath: "/api/v1",
    schemes: ["http", "https"],
    produces: ["application/json"],
    components: {
      schemas: schemas,
    },
    securityDefinitions: {
      authorization: {
        type: "apiKey",
        name: "Authorization",
        in: "header",
      },
    },
  },
  // ['.routes/*.js']
  apis: [
    "./src/routes/v1/index.js",
    "./src/routes/v1/partials/auth.js",
    "./src/routes/v1/partials/module.js",
    "./src/routes/v1/partials/action.js",
    "./src/routes/v1/partials/zone.js",
    "./src/routes/v1/partials/role.js",
    "./src/routes/v1/partials/user.js",
    "./src/routes/v1/partials/user-role.js",
    "./src/routes/v1/partials/user-log.js",
    "./src/routes/v1/partials/file.js",
    "./src/routes/v1/partials/file-category.js",
  ],
};
