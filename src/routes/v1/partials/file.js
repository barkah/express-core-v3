const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getMimeType,
  dataFile,
  getCategory,
  getDefault,
  postFile,
  deleteFile,
  loadFile,
} = require("../../../app/controllers/file");

/**
 * @swagger
 *
 * /file/mime-type:
 *  get:
 *    tags:
 *      - "file"
 *    responses:
 *      '200':
 *          schema:
 *              type: "object"
 *              example: {"123": "application/vnd.lotus-1-2-3", "1km": "application/vnd.1000minds.decision-model+xml", "3dml": "text/vnd.in3d.3dml",}
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/file/mime-type", callback(getMimeType));

router.get("/file/default", callback(getDefault));

router.post("/file/upload/:fileCategory?", postFile);

router.post("/file/load", callback(loadFile));

/**
 * @swagger
 *
 * /file/category:
 *  get:
 *    tags:
 *      - "file"
 *    responses:
 *      '200':
 *          schema:
 *              type: "object"
 *              example: {"allowedExtension": ["image/jpeg", "image/jpeg", "image/png"], "id": "3b96aa7b-6e5b-4da7-9cc1-4f21d654c17a", "maxFileSize": 2048}
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/file/category", callback(getCategory));

/**
 * @swagger
 *
 * /file/data:
 *  post:
 *    tags:
 *      - "file"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/file/data", callback(dataFile));

/**
 * @swagger
 *
 * /file/delete:
 *  delete:
 *    tags:
 *      - "file"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Delete File"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/file/delete", callback(deleteFile));

module.exports = router;
