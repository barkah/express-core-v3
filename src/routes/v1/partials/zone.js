const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getZone,
  dataZone,
  postZone,
  patchZone,
  deleteZone,
  getZoneLevel,
} = require("../../../app/controllers/zone");

/**
 * @swagger
 *
 * /zone:
 *  get:
 *    tags:
 *      - "zone"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Zone ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/zoneData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/zone", callback(getZone));

/**
 * @swagger
 *
 * /zone:
 *  post:
 *    tags:
 *      - "zone"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Zone Baru"
 *        schema:
 *          $ref: "#/components/schemas/zoneData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/zone", callback(postZone));

/**
 * @swagger
 *
 * /zone:
 *  patch:
 *    tags:
 *      - "zone"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Zone ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Zone"
 *        schema:
 *          $ref: "#/components/schemas/zoneData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/zone", callback(patchZone));

/**
 * @swagger
 *
 * /zone:
 *  delete:
 *    tags:
 *      - "zone"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Zone ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/zone", callback(deleteZone));

/**
 * @swagger
 *
 * /zone/data:
 *  post:
 *    tags:
 *      - "zone"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, code, name, phone, address, lat, lng, level, parent, active</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/zone/data", callback(dataZone));

/**
 * @swagger
 *
 * /zone/level:
 *  get:
 *    tags:
 *      - "zone"
 *    responses:
 *      '200':
 *          schema:
 *              type: "object"
 *              example: [{ value: "kelurahan", text: "Kelurahan" }]
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/zone/level", callback(getZoneLevel));

module.exports = router;
