const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getRole,
  dataRole,
  postRole,
  patchRole,
  deleteRole,
  getListRole,
} = require("../../../app/controllers/role");

/**
 * @swagger
 *
 * /role:
 *  get:
 *    tags:
 *      - "role"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Role ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/roleData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/role", callback(getRole));

/**
 * @swagger
 *
 * /role:
 *  post:
 *    tags:
 *      - "role"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Role Baru"
 *        schema:
 *          $ref: "#/components/schemas/roleData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/role", callback(postRole));

/**
 * @swagger
 *
 * /role:
 *  patch:
 *    tags:
 *      - "role"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Role ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Role"
 *        schema:
 *          $ref: "#/components/schemas/roleData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/role", callback(patchRole));

/**
 * @swagger
 *
 * /role:
 *  delete:
 *    tags:
 *      - "role"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Role ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/role", callback(deleteRole));

/**
 * @swagger
 *
 * /role/data:
 *  post:
 *    tags:
 *      - "role"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name, default_route, permissions</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/role/data", callback(dataRole));

/**
 * @swagger
 *
 * /role/list:
 *  get:
 *    tags:
 *      - "role"
 *    responses:
 *      '200':
 *          schema:
 *            type: "object"
 *            example: [{"id":"", "slug":"", "name":""}]
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/role/list", callback(getListRole));

module.exports = router;
