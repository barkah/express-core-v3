"use strict";
const moment = require("moment");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const date = moment().format("YYYY-MM-DD HH:mm:ss");

    // Seeding Zone
    await queryInterface.bulkInsert("zone", [
      {
        id: "26201546-682c-4bf1-9d0a-8324af031d39",
        code: "default",
        name: "Default Zone",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
    ]);

    // Seeding Actions
    await queryInterface.bulkInsert("action", [
      {
        id: "98f043e9-b33f-46ce-8bd9-a1ee45af1874",
        slug: "create",
        name: "Create",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
      {
        id: "558ac193-87d7-4bd5-b0a2-607ca97cb59d",
        slug: "update",
        name: "Update",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
      {
        id: "e3d031bb-7659-4373-a351-1016b7cb4918",
        slug: "delete",
        name: "Delete",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
      {
        id: "a6641b6f-28bb-46b6-beac-b0c066368176",
        slug: "view",
        name: "View",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
    ]);

    // Seeding Module
    await queryInterface.bulkInsert("module", [
      {
        id: "528ab6b3-1848-48c7-8d83-5e64ae7089e5",
        slug: "user",
        name: "User",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
      {
        id: "d7a06989-140f-4a32-a12b-1997ddfac05d",
        slug: "role",
        name: "Role",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
      {
        id: "8def8678-fbfe-4bef-907d-6990eb7a2344",
        slug: "action",
        name: "Action",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
      {
        id: "91236c57-ee30-4562-9391-4a90f8eda88d",
        slug: "module",
        name: "Module",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
      {
        id: "c0c595d8-fc6d-4626-8d22-d9cf27d87816",
        slug: "zone",
        name: "Zone",
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
    ]);

    // Seeding Role
    await queryInterface.bulkInsert("role", [
      {
        id: "c2a88b18-4783-4fcc-8192-db32ec9aa297",
        slug: "administrator",
        name: "Administrator",
        default_route: "/",
        permissions:
          '{"user":["view","create","update","delete","change-password"],"role":["view","create","update","delete"],"action":["view","create","update","delete"],"module":["view","create","update","delete"],"zone":["view","create","update","delete"]}',
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
    ]);

    // Seeding User
    await queryInterface.bulkInsert("user", [
      {
        id: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        name: "Demo",
        email: "demo@demo.com",
        password:
          "$2a$12$htvEwSodqjlSP3l.GZhjQOY2MKmWO1yma8OmEuDUeIBwMhAm8M3ly",
        active: true,
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
    ]);

    // Seeding User Roles
    await queryInterface.bulkInsert("user_role", [
      {
        id: "512ee389-a412-4064-b47f-9faec55922fb",
        user_id: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        zone_id: "26201546-682c-4bf1-9d0a-8324af031d39",
        roles: '["administrator"]',
        created_by: "54de80f4-fba4-4fcc-874f-2672a03f468e",
        created_at: date,
        updated_at: date,
      },
    ]);
  },

  down: (queryInterface, Sequelize) => {},
};
