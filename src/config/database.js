const config = {
  dialect: "postgres",
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  schema: "public",
  dialectOptions: {
    useUTC: false, // for reading from database
  },
  timezone: "Asia/Jakarta",
};

module.exports = {
  development: config,
  test: config,
  production: config,
};
