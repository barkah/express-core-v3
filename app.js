const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const config = require("./src/config/app.js");
const userAgent = require("express-useragent");
const timeout = require("connect-timeout");
const app = express();

app.use(userAgent.express());

// Setup Cors
var corsOptionsDelegate = function (req, callback) {
  var origin = false;
  if (config.whitelistUrl.indexOf(req.header("Origin")) !== -1) {
    origin = true;
  }

  const corsOptions = {
    origin: origin,
    credentials: true,
    methods: ["GET", "PUT", "PATCH", "POST", "DELETE"],
    optionsSuccessStatus: 200,
  };
  callback(null, corsOptions);
};

app.use(cors(corsOptionsDelegate));

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// Set Timeout
app.use(timeout(config.timeout));
app.use((req, _, next) => {
  if (!req.timedout) next();
});

if (config.apiRoot != null) {
  app.use(config.apiRoot, require("./src/routes"));
} else {
  app.use(require("./src/routes"));
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404);
  res.json({ statusCode: 404, message: "Page not found" });
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
